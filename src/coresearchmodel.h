/*
    *
    * This file is a part of CoreHunt.
    * A file search utility for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/


#pragma once

#include <QAbstractItemModel>
#include <QModelIndex>
#include <QMap>
#include <QIcon>

#include <cprime/filefunc.h>


class CoreSearchItem {

public:
	explicit CoreSearchItem(const QString &iconStr, const QString &filePath, const QString &type) {
        mIcon = QIcon::fromTheme(iconStr);
        mName = CPrime::FileUtils::baseName(filePath);
        mPath = filePath;
        mType = type;
    }

    QString name() {
        return mName;
    }

    QString path() {
        return mPath;
    }

    QString type() {
        return mType;
    }

    QIcon icon() {
        return mIcon;
    }

private:
    QIcon mIcon;
    QString mName, mPath, mType;

};

typedef QList<CoreSearchItem *> CoreSearchItemList;

class CoreSearchModel : public QAbstractItemModel {

    Q_OBJECT

public:
	explicit CoreSearchModel();

    QModelIndex index(int, int, const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &parent = QModelIndex()) const override;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    /* Set a category from which the data is shown */
	bool setCategory(const QString &);

    /* Add a category into which data will be sorted */
	bool addCategory(const QString &) const;

    /* Add search results @items to @category*/
	void addResults(const QString &category, CoreSearchItemList &items);

    /* Add search results @items to @category*/
	void addResult(const QString &category, CoreSearchItem *item);

    /* Clear all the data */
    void clear();

    /* List of categories */
    static QStringList categories;

private:
    /* This is where we store all the results */
    mutable QMap<QString, CoreSearchItemList> categoryItemMap;

    /* Current category */
    mutable QString mCategory = "All";

};
