/*
    *
    * This file is a part of CoreHunt.
    * A file search utility for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#include <QCommandLineParser>
#include <QFileInfo>
#include <QApplication>
#include <QDebug>

#include "corehunt.h"


int main(int argc, char *argv[])
{
	QApplication app(argc, argv);

	// Set application info
	app.setOrganizationName("CuboCore");
	app.setApplicationName("CoreHunt");
	app.setApplicationVersion(QStringLiteral(VERSION_TEXT));
    app.setDesktopFileName("cc.cubocore.CoreHunt.desktop");
    app.setQuitOnLastWindowClosed(true);

	QCommandLineParser parser;
	parser.addHelpOption();         // Help
	parser.addVersionOption();      // Version

	/* Optional: Path where the search begins */
	parser.addOption({ "path", "The path where the search begins.", "path" });

	/* Optional: Pattern we are searching for */
	parser.addOption({ "pattern", "Pattern which we are searching for.", "pattern" });

	/* No UI flag */
	parser.addOption({ "cli", "Use CLI only.", "" });

	/* Process the CLI args */
	parser.process(app);

	QString path = parser.value("path");

    if (path.length()) {
		QFileInfo fi(path);
		if (fi.isFile())
			path = fi.absolutePath();
		else
			path = fi.absoluteFilePath();
	}

	// If positional argument (path) provided while others are unset
	// Used mostly for open folder with
	if (!parser.isSet("cli") and !parser.isSet("path")) {
		if (parser.positionalArguments().count()) {
			QFileInfo fi(parser.positionalArguments().at(0));

			path = fi.absoluteFilePath();

			if (fi.isFile())
				path = fi.absolutePath();
		} else {
			path = "";
		}
	}

	/* Initialize the app */
	corehunt hunt(path, parser.value("pattern"));

	if (parser.isSet("cli")) {
		if (not (parser.isSet("path") and parser.isSet("pattern"))) {
			CoreHuntIO::cout << "CoreHunt " << QString(VERSION_TEXT) << CoreHuntIO::endl;
			CoreHuntIO::cerr << "[ERROR:] Please specify path and pattern." << CoreHuntIO::endl;

			return 1;
		}

		hunt.setUseCli(true);
		hunt.search();

		return 0;
	}

	hunt.show();

	return app.exec();
}
