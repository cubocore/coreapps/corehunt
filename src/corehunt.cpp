/*
    *
    * This file is a part of CoreHunt.
    * A file search utility for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/


#include <QScroller>
#include <QDirIterator>
#include <QFileDialog>
#include <QElapsedTimer>
#include <QShortcut>
#include <QDir>
#include <QRegularExpression>

#include <cprime/appopenfunc.h>
#include <cprime/sortfunc.h>
#include <cprime/themefunc.h>
#include <cprime/systemxdg.h>
#include <cprime/variables.h>

#include "coresearchmodel.h"
#include "settings.h"

#include "corehunt.h"
#include "ui_corehunt.h"

/* SystemXdgMime instance */
CPrime::SystemXdgMime *mimeM = CPrime::SystemXdgMime::instance();
QStringList corehunt::documentTypes = QStringList();


corehunt::corehunt(const QString &path, const QString &pattern, QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::corehunt)
	, smi(new settings)
{
	ui->setupUi(this);

	QPalette pltt(palette());
	pltt.setColor(QPalette::Base, Qt::transparent);
	setPalette(pltt);

	documentTypes << "pdf" << "doc" << "docx" << "ppt" << "pptx" << "xls" << "xlsx" << "odt" << "odp" << "ods" << "rtf" << "djv" << "djvu";
	searchActFilePath = QDir(CPrime::Variables::CC_Library_ConfigDir()).filePath("searchactivity");

	loadSettings();
	startSetup();
    setupIcons();
	shotcuts();
	reloadHistory();

    if (path.length() and CPrime::FileUtils::exists(path)) {
		ui->page->setCurrentIndex(0);
		ui->path->setText(path);
		on_tools_clicked();
	}

    if (pattern.length()) {
		ui->searchLE->setText(pattern);
		ui->page->setCurrentIndex(0);
	}
}

corehunt::~corehunt()
{
	if (mIsActive) {
		on_stop_clicked();
	}

	delete smi;
	delete ui;
}

/**
 * @brief Setup ui elements
 */
void corehunt::startSetup()
{

    // all toolbuttons icon size in sideView
    QList<QToolButton *> sideViewButtons = ui->sideView->findChildren<QToolButton *>();
    for (int i = 0; i < sideViewButtons.size(); ++i) {
        QToolButton *b = sideViewButtons.at(i);
        if (b) {
            b->setIconSize(toolsIconSize);
        }
    }


    // all toolbuttons icon size in topBar
    QList<QToolButton *> topBarButtons = ui->topBar->findChildren<QToolButton *>();
    for (int i = 0; i < topBarButtons.size(); ++i) {
        QToolButton *b = topBarButtons.at(i);
        if (b) {
            b->setIconSize(toolsIconSize);
        }
    }


    // all toolbuttons icon size in toolBar
    QList<QToolButton *> toolBarButtons = ui->toolBar->findChildren<QToolButton *>();
    for (int i = 0; i < toolBarButtons.size(); ++i) {
        QToolButton *b = toolBarButtons.at(i);
        if (b) {
            b->setIconSize(toolsIconSize);
        }
    }


	ui->results->setIconSize(listViewIconSize);
	ui->results->verticalHeader()->setDefaultSectionSize(listViewIconSize.width() + 8);
	ui->activity->setIconSize(listViewIconSize);
	ui->path->setText(QDir::homePath());
	ui->find->setDisabled(true);
	ui->stop->hide();
	ui->toolBar->setVisible(false);

	QScroller::grabGesture(ui->activity, QScroller::LeftMouseButtonGesture);
	QScroller::grabGesture(ui->results, QScroller::LeftMouseButtonGesture);

	ui->menu->setVisible(0);
	ui->appTitle->setAttribute(Qt::WA_TransparentForMouseEvents);
    ui->appTitle->setFocusPolicy(Qt::NoFocus);
    this->resize(800, 500);

    if (uiMode == 2) {
        // setup mobile UI

        this->setWindowState(Qt::WindowMaximized);
        connect(ui->menu, &QToolButton::clicked, this, &corehunt::showSideView);
        connect(ui->appTitle, &QToolButton::clicked, this, &corehunt::showSideView);

        ui->sideView->setVisible(0);
        ui->menu->setVisible(1);
    } else {
        // setup desktop or tablet UI

        if(windowMaximized){
            this->setWindowState(Qt::WindowMaximized);
            qDebug() << "window is maximized";
        } else{
            this->resize(windowSize);
        }
    }

	ui->page->setCurrentIndex(0);

	model = new CoreSearchModel();
	ui->results->setModel(model);
	ui->results->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
	ui->results->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);

	if (uiMode != 0) {
		connect(ui->results, SIGNAL(clicked(const QModelIndex &)), this, SLOT(openResults(const QModelIndex &)));
		connect(ui->activity, SIGNAL(clicked(const QModelIndex &)), this, SLOT(searchAgain(const QModelIndex &)));
	} else {
		connect(ui->results, SIGNAL(doubleClicked(const QModelIndex &)), this, SLOT(openResults(const QModelIndex &)));
		connect(ui->activity, SIGNAL(doubleClicked(const QModelIndex &)), this, SLOT(searchAgain(const QModelIndex &)));
	}

	ui->page->setCurrentIndex(1);
	ui->appPage->setCurrentIndex(1);
}

void corehunt::setupIcons(){
    ui->folder->setIcon(CPrime::ThemeFunc::themeIcon( "document-open-symbolic", "quickopen-file", "document-open-symbolic" ));
    ui->text->setIcon(CPrime::ThemeFunc::themeIcon( "text-editor-symbolic", "format-text", "text-editor" ));
    ui->image->setIcon(CPrime::ThemeFunc::themeIcon( "folder-pictures-symbolic", "bitmap-trace", "bitmap-trace" ));
    ui->documents->setIcon(CPrime::ThemeFunc::themeIcon( "document-new-symbolic", "document-new", "document-new" ));
    ui->tools->setIcon(CPrime::ThemeFunc::themeIcon( "view-more-symbolic", "tools-wizard", "view-more-symbolic" ));
    ui->menu->setIcon(CPrime::ThemeFunc::themeIcon( "open-menu-symbolic", "application-menu", "open-menu" ));
    ui->media->setIcon(CPrime::ThemeFunc::themeIcon( "media-playback-start-symbolic", "media-playback-start", "media-playback-start" ));
    ui->all->setIcon(CPrime::ThemeFunc::themeIcon( "document-properties-symbolic", "document-properties", "document-properties" ));
    ui->others->setIcon(CPrime::ThemeFunc::themeIcon( "document-edit-symbolic", "edit-rename", "document-edit" ));
    ui->find->setIcon(CPrime::ThemeFunc::themeIcon( "edit-find-symbolic", "edit-find", "edit-find" ));
    ui->stop->setIcon(CPrime::ThemeFunc::themeIcon( "media-playback-stop-symbolic", "media-playback-stop", "media-playback-stop" ));
    ui->browse->setIcon(CPrime::ThemeFunc::themeIcon( "document-open-symbolic", "quickopen-file", "document-open-symbolic" ));
}

void corehunt::setUseCli(bool ok)
{
	mCliMode = ok;
}

void corehunt::search()
{
	CoreHuntIO::cout << "CoreHunt " << qApp->applicationVersion() << CoreHuntIO::endl;
	CoreHuntIO::cout << "Searching for '" << ui->searchLE->text() << "' in: " << ui->path->text() << CoreHuntIO::endl << CoreHuntIO::endl;

	Qt::flush(CoreHuntIO::cout);

	on_find_clicked();
}

void corehunt::loadSettings()
{
    // get CSuite's settings
    toolsIconSize = smi->getValue("CoreApps", "ToolsIconSize");
    uiMode = smi->getValue("CoreApps", "UIMode");
    activities = smi->getValue("CoreApps", "KeepActivities");
    listViewIconSize = smi->getValue("CoreApps", "ListViewIconSize");

    // get app's settings
    windowSize = smi->getValue("CoreHunt", "WindowSize");
    windowMaximized = smi->getValue("CoreHunt", "WindowMaximized");
}

void corehunt::showSideView()
{
	if (uiMode == 2) {
		if (ui->sideView->isVisible()) {
			ui->sideView->setVisible(0);
		} else {
			ui->sideView->setVisible(1);
		}
	}
}

void corehunt::shotcuts()
{
	QShortcut *shortcut;

	shortcut = new QShortcut(QKeySequence(Qt::Key_Enter), this);
	connect(shortcut, &QShortcut::activated, this, &corehunt::on_find_clicked);

	shortcut = new QShortcut(QKeySequence(Qt::ControlModifier | Qt::Key_O), this);
	connect(shortcut, &QShortcut::activated, this, &corehunt::on_browse_clicked);

	shortcut = new QShortcut(QKeySequence(Qt::ControlModifier | Qt::Key_S), this);
	connect(shortcut, &QShortcut::activated, this, &corehunt::on_stop_clicked);
}

void corehunt::show()
{
	QWidget::show();
	qApp->processEvents();

	if (not ui->searchLE->text().isEmpty()) {
		on_find_clicked();
	}
}

void corehunt::changeCategory(int i)
{
	model->setCategory(CoreSearchModel::categories[ i ]);
}

void corehunt::resetUI()
{
	ui->searchLE->setEnabled(true);
	ui->path->setEnabled(true);
	ui->browse->setEnabled(true);

	// Clear the result list and time
	model->clear();
	ui->searchTime->clear();

	/* Final text of all the category buttons */
	ui->all->setText("All (0 item)");
	ui->folder->setText("Folders (0 item)");
	ui->image->setText("Images (0 item");
	ui->media->setText("Media (0 item)");
	ui->documents->setText("Documents (0 item)");
	ui->text->setText("Text (0 item)");
	ui->others->setText("Others (0 item)");

	ui->find->show();
	ui->stop->hide();
}

QString corehunt::sentDateText(const QString &dateTime)
{
	QDateTime given = QDateTime::fromString(dateTime, "dd.MM.yyyy");

	if (QDate::currentDate().toString("dd.MM.yyyy") == dateTime) {
		return QString("Today");
	} else {
		return QString(given.toString("MMMM dd"));
	}
}

void corehunt::reloadHistory()
{
    if (not activities) {
		ui->appPage->setCurrentIndex(0);
		ui->startSearch->setFocus();
		QFile(searchActFilePath).remove();

		return;
	}

	// Search Activity file Path
	QString sActFile = searchActFilePath;
	QSettings searchActF(sActFile, QSettings::IniFormat);

	ui->activity->clear();
	QStringList toplevel = searchActF.childGroups();

	int itemTemp = toplevel.count();

	if (!itemTemp) {
		ui->appPage->setCurrentIndex(0);
		ui->startSearch->setFocus();
		return;
	}

	toplevel = CPrime::SortFunc::sortDate(toplevel);

	foreach (QString group, toplevel) {
		QTreeWidgetItem *topTree = new QTreeWidgetItem;

		QString groupL = sentDateText(group);
		topTree->setText(0, groupL);

		searchActF.beginGroup(group);

		QStringList keys = searchActF.childKeys();

		keys = CPrime::SortFunc::sortTime(keys, Qt::DescendingOrder, "hh.mm.ss");

		foreach (QString key, keys) {
			QTreeWidgetItem *child = new QTreeWidgetItem;
			QString value = searchActF.value(key).toString();
			child->setText(0, value);
			topTree->addChild(child);
		}

		searchActF.endGroup();
		ui->activity->insertTopLevelItem(0, topTree);
	}

	if (toplevel.count()) {
		ui->activity->setExpanded(ui->activity->model()->index(0, 0), true);
	}

	// set focus to search box
	ui->searchLE->setFocus();
}

void corehunt::searchAgain(const QModelIndex &index)
{
	QString str = index.data().toString();
	QStringList list = str.split("\t\t\t");

	if (list.count() < 2) {
		return;
	}

	ui->searchLE->setText(list[0]);
	ui->path->setText(list[1]);

	on_find_clicked();
}

void corehunt::openFile(const QModelIndex &idx)
{
	QString file = idx.data().toString();
	CoreHuntIO::cout << file.toLatin1().data() << CoreHuntIO::endl;
	Qt::flush(CoreHuntIO::cout);

	CPrime::AppOpenFunc::appOpenEngine(file);
}

void corehunt::on_searchLE_textChanged(const QString &text)
{
    if (text.length())  {
		ui->find->setEnabled(true);
	} else {
		ui->page->setCurrentIndex(1);
		ui->find->setEnabled(false);

		resetUI();
		reloadHistory();
	}
}

void corehunt::on_find_clicked()
{
	startSearch(ui->searchLE->text());
}

void corehunt::on_stop_clicked()
{
	/* Set the flag to stop the search */
	mStopSearch = true;

	/* Enable the search and path line edits */
	ui->searchLE->setEnabled(true);
	ui->path->setEnabled(true);
	ui->browse->setEnabled(true);

	/* Show find button, hide stop button */
	ui->find->show();
	ui->stop->hide();
}

void corehunt::on_browse_clicked()
{
	QString newPath = QFileDialog::getExistingDirectory(this, "CoreHunt - Choose Directory", ui->path->text());
    ui->path->setText(newPath.length() ? newPath : ui->path->text());
}

void corehunt::startSearch(const QString &keyword)
{
	if (keyword.isEmpty() || ui->path->text().isEmpty()) {
		return;
	}

	resetUI();

	ui->appPage->setCurrentIndex(1);
	ui->page->setCurrentIndex(0);

	/* Save search history */
	QSettings searchHistory(searchActFilePath, QSettings::IniFormat);

	QDateTime now = QDateTime::currentDateTime();
	searchHistory.beginGroup(now.toString("dd.MM.yyyy"));
	searchHistory.setValue(now.toString("hh.mm.ss"), QString(keyword + "\t\t\t" + ui->path->text()));
	searchHistory.endGroup();
	searchHistory.sync();

	/* Disable and enable text edits */
	ui->searchLE->setDisabled(true);
	ui->path->setDisabled(true);
	ui->browse->setDisabled(true);

	/* Reste category button text */
	//    for( int i = 0; i < CoreSearchModel::categories.count(); i++ )
	//        ctgGroup->button( i )->setText( CoreSearchModel::categories[ i ] );

	/* Hide find button; show stop button */
	ui->find->hide();
	ui->stop->show();

	// make & populate the result
	makeSearchResults(keyword);

	/* Enable the search and path line edits */
	ui->searchLE->setEnabled(true);
	ui->path->setEnabled(true);
	ui->browse->setEnabled(true);

	/* Show find button, hide stop button */
	ui->find->show();
	ui->stop->hide();
}

void corehunt::makeSearchResults(const QString &keyword)
{
	// Start timer
	QElapsedTimer *timer = new QElapsedTimer;
	timer->start();

	/* Set process active */
	mIsActive = true;

	/* Obtain pattern from line edit */
    QString wildcardExp = QRegularExpression::wildcardToRegularExpression(keyword);
	QRegularExpression pattern(QRegularExpression::anchoredPattern(wildcardExp), QRegularExpression::CaseInsensitiveOption);

	/* Init counters */
	int all = 0, dirs = 0, imgs = 0, media = 0, docs = 0, txt = 0, others = 0;

	/* Create QDirIterator */
	QDirIterator it(ui->path->text(), QDir::AllEntries | QDir::System | QDir::NoDotAndDotDot | QDir::Hidden, QDirIterator::Subdirectories);

	/* Start looping through existing files /folders */
	while (it.hasNext()) {
		/* If stop button has been clicked, stop search */
		if (mStopSearch) {
			mStopSearch = false;
			mIsActive = false;
			return;
		}

		/* Get the next dir entry */
		it.next();

		/* Process UI event to make sure it does not hang */
		qApp->processEvents();

		/* Entry's QFileInfo */
		QFileInfo info(it.fileInfo());

		/* If entry name contains the pattern, add it to the model */
		if (info.fileName().contains(pattern)) {

			/* Get the entry's mimetype for showing file tpye */
			QMimeType mime = mimeDbInstance.mimeTypeForFile(info.filePath());

			/* If we are in the cli mode, print it and exit */
            if (mCliMode) {
				CoreHuntIO::cout << mime.comment().leftJustified(25, ' ', true) << "    " << info.filePath() << CoreHuntIO::endl;
				Qt::flush(CoreHuntIO::cout);
				continue;
			}

			/* Prepare item to be added to the model */
			CoreSearchItem *item = new CoreSearchItem(mime.iconName(), info.filePath(), mime.comment());

			/* Automatically add */
			model->addResult("All", item);
			all += 1;
			ui->all->setText(QString("All (%1 item(s))").arg(all));

			/* Folders */
			if (mime.name() == "inode/directory") {
				model->addResult("Folders", item);
				dirs += 1;
				ui->folder->setText(QString("Folders (%1 item%2)").arg(dirs).arg(dirs != 1 ? "s" : ""));
			}

			/* Document? */
			else if (documentTypes.contains(mime.preferredSuffix())) {
				model->addResult("Documents", item);
				docs += 1;
				ui->documents->setText(QString("Folders (%1 item%2)").arg(docs).arg(docs != 1 ? "s" : ""));
			}

			/* Image */
			else if (mime.name().startsWith("image/")) {
				model->addResult("Images", item);
				imgs += 1;
				ui->image->setText(QString("Images (%1 item%2)").arg(imgs).arg(imgs != 1 ? "s" : ""));
			}

			/* Audio */
			else if (mime.name().startsWith("audio/")) {
				model->addResult("Media", item);
				media += 1;
				ui->media->setText(QString("Media (%1 item%2)").arg(media).arg(media != 1 ? "s" : ""));
			}

			/* Video */
			else if (mime.name().startsWith("video/")) {
				model->addResult("Media", item);
				media += 1;
				ui->media->setText(QString("Media (%1 item%2)").arg(media).arg(media != 1 ? "s" : ""));
			}

			/* Text */
			else if (mime.name().startsWith("text/")) {
				model->addResult("Text", item);
				txt += 1;
				ui->text->setText(QString("Text (%1 item%2)").arg(txt).arg(txt != 1 ? "s" : ""));
			}

			/* Others */
			else {
				model->addResult("Others", item);
				others += 1;
				ui->others->setText(QString("Others (%1 item%2)").arg(others).arg(others != 1 ? "s" : ""));
			}

			qApp->processEvents();
		}
	}

	/* Search is over enable the UI */

	/* Final text of all the category buttons */
	ui->all->setText(QString("All (%1 item%2)").arg(all).arg(all != 1 ? "s" : ""));
	ui->folder->setText(QString("Folders (%1 item%2)").arg(dirs).arg(dirs != 1 ? "s" : ""));
	ui->image->setText(QString("Images (%1 item%2)").arg(imgs).arg(imgs != 1 ? "s" : ""));
	ui->media->setText(QString("Media (%1 item%2)").arg(media).arg(media != 1 ? "s" : ""));
	ui->documents->setText(QString("Documents (%1 item%2)").arg(docs).arg(docs != 1 ? "s" : ""));
	ui->text->setText(QString("Text (%1 item%2)").arg(txt).arg(txt != 1 ? "s" : ""));
	ui->others->setText(QString("Others (%1 item%2)").arg(others).arg(others != 1 ? "s" : ""));

	// Show time
	qint64 msec = timer->elapsed(); // milisec
	QTime time = QTime::fromMSecsSinceStartOfDay(msec);
	QString timetxt = time.toString("ss.zzz") + " seconds ";

	if (time.hour()) {
		timetxt = time.toString("hh:mm:ss.zzz") + " hours ";
	} else if (time.minute()) {
		timetxt = time.toString("mm:ss.zzz") + " minutes ";
	}

	ui->searchTime->setText(QString("About %1 results within ").arg(ui->results->model()->rowCount()) + timetxt);

}
void corehunt::openResults(const QModelIndex &index)
{
	QString file = index.data().toString();
	CPrime::AppOpenFunc::appOpenEngine(file);
}

void corehunt::categoryClick(QToolButton *btn, int i)
{

	// first do this to get all the space for contents
	if (uiMode == 2) {
		ui->sideView->setVisible(0);
	}

    // all button checked false
    QList<QToolButton *> sideViewButtons = ui->sideView->findChildren<QToolButton *>();
    for (int i = 0; i < sideViewButtons.size(); ++i) {
        QToolButton *b = sideViewButtons.at(i);
        b->setChecked(false);
    }


	btn->setChecked(true);
	changeCategory(i);
}

void corehunt::on_all_clicked()
{
	categoryClick(ui->all, 0);
}

void corehunt::on_folder_clicked()
{
	categoryClick(ui->folder, 1);
}

void corehunt::on_image_clicked()
{
	categoryClick(ui->image, 2);
}

void corehunt::on_media_clicked()
{
	categoryClick(ui->media, 3);
}

void corehunt::on_documents_clicked()
{
	categoryClick(ui->documents, 4);
}

void corehunt::on_text_clicked()
{
	categoryClick(ui->text, 5);
}

void corehunt::on_others_clicked()
{
	categoryClick(ui->others, 6);
}

void corehunt::on_tools_clicked()
{
	if (ui->toolBar->isVisible()) {
		ui->toolBar->setVisible(false);
	} else {
		ui->toolBar->setVisible(true);
	}
}

void corehunt::on_cleanHistory_clicked()
{
	ui->activity->clear();

	QSettings searchHistory(searchActFilePath, QSettings::IniFormat);
	searchHistory.clear();
	searchHistory.sync();

	reloadHistory();
}

void corehunt::on_searchLE_returnPressed()
{
	on_find_clicked();
}

void corehunt::on_search_clicked()
{
	ui->searchLE->setText(ui->startSearch->text());
	startSearch(ui->startSearch->text());
    ui->startSearch->clear();
}

void corehunt::on_startSearch_returnPressed()
{
	ui->searchLE->setText(ui->startSearch->text());
	startSearch(ui->startSearch->text());
    ui->startSearch->clear();
}


void corehunt::closeEvent(QCloseEvent *cEvent)
{
    qDebug()<< "save window stats"<< this->size() << this->isMaximized();

    smi->setValue("CoreHunt", "WindowSize", this->size());
    smi->setValue("CoreHunt", "WindowMaximized", this->isMaximized());
}
