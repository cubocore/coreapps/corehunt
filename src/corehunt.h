/*
    *
    * This file is a part of CoreHunt.
    * A file search utility for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, vsit http://www.gnu.org/licenses/.
    *
*/

#pragma once

#include <QWidget>
#include <QTextStream>

class QToolButton;
class CoreSearchModel;
class settings;

namespace Ui {
    class corehunt;
}

namespace CoreHuntIO {
    /* Console stdout ouput */
    static QTextStream cout(stdout);

    /* Console stdout ouput */
    static QTextStream cerr(stderr);

    /* New Line */
    static QChar endl('\n');
}

class corehunt : public QWidget {
    Q_OBJECT

public:
	explicit corehunt(const QString &path = QString(), const QString &pattern = QString(), QWidget *parent = nullptr);
    ~corehunt();

    void setUseCli(bool);
    void search();

public Q_SLOTS:
	void show();

protected:
    void closeEvent(QCloseEvent *cEvent) override;

private Q_SLOTS:
    void changeCategory(int);
    void resetUI();
    void openFile(const QModelIndex &);
    void on_searchLE_textChanged(const QString &text);
    void on_find_clicked();
    void on_stop_clicked();
    void on_browse_clicked();
    void openResults(const QModelIndex &index);
    void on_all_clicked();
    void on_folder_clicked();
    void on_image_clicked();
    void on_media_clicked();
    void on_documents_clicked();
    void on_text_clicked();
    void on_others_clicked();
    void searchAgain(const QModelIndex &index);
    void on_tools_clicked();
    void on_cleanHistory_clicked();
    void on_searchLE_returnPressed();
	void startSearch(const QString &keyword);
    void on_search_clicked();
    void on_startSearch_returnPressed();
    void showSideView();

private:
    Ui::corehunt   *ui;
    bool            activities, windowMaximized;
    int             uiMode;
    QSize           toolsIconSize, listViewIconSize, windowSize;
    bool            mIsActive = false;
    bool            mStopSearch = false;
    bool            mCliMode = false;
    CoreSearchModel *model;
    static QStringList documentTypes;
	settings *smi;
	QString searchActFilePath;

    void calculateElapsed();
    void loadSettings();
    void reloadHistory();
    void startSetup();
    void setupIcons();
    void categoryClick(QToolButton *btn, int i);
    void shotcuts();
	void makeSearchResults(const QString &keyword);
    QString sentDateText(const QString &dateTime);
};
